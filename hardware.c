/*
 * File:   hardware.c
 * Author: 
 *
 */


#include "hardware.h"
#include "app_config.h"
#include "mcc_generated_files/mcc.h"

inline void Hardware_Init(void) {
    SYSTEM_Initialize();
}

/*
 * Disable Global and Peripheral Interrupts so interrupt handlers will not be
 * called. Interrupt flag bits are still set when an interrupt condition occurs
 * and we will use polling to handle them instead. 
 */
inline void Hardware_Disable_Interrupt_Handler(void) {
    INTERRUPT_GlobalInterruptDisable();
    INTERRUPT_PeripheralInterruptDisable();
}

/*
 * Disable Watchdog by setting SWDTEN bit to 0
 */
inline void Hardware_Disable_Watchdog(void) {
    WDTCONbits.SWDTEN = 0;
}

/*
 * Enable Watchdog by setting SWDTEN bit to 1
 */
inline void Hardware_Enable_Watchdog(void) {
    WDTCONbits.SWDTEN = 1;
}

/*
 * Clear Watchdog
 */
inline void Hardware_Clear_Watchdog(void) {
    CLRWDT();
}

/*
 * To disable the CAN Transceiver controlled by CAN0_STBY_N
 */
inline void Hardware_Disable_CAN_Transceiver(void) {
    CAN0_STBY_N_SetLow();
}

/*
 * To enable the CAN Transceiver controlled by CAN0_STBY_N
 */
inline void Hardware_Enable_CAN_Transceiver(void) {
    CAN0_STBY_N_SetHigh();
}

/*
 * To give away the control of CAN Transceiver by setting CAN0_STBY_N as input
 */
inline void Hardware_Give_CAN_Transceiver(void) {
    CAN0_STBY_N_SetDigitalInput();
}

/*
 * To take over the control of CAN Transceiver by setting CAN0_STBY_N as output
 */
inline void Hardware_Take_CAN_Transceiver(void) {
    CAN0_STBY_N_SetDigitalOutput();
}

/*
 * To give away the control of CAN bus by setting CAN0_TX as input
 */
inline void Hardware_Give_CAN_Bus(void) {
    RB2_SetHigh(); // set high then tri-state
    RB2_SetDigitalInput(); // CAN0_TX, tri-state
}

/*
 * To take over the control of CAN bus by setting CAN0_TX as output
 */
inline void Hardware_Take_CAN_Bus(void) {
    RB2_SetHigh();
    RB2_SetDigitalOutput(); // CAN0_TX, drive output
}

/*
 * To disable FPGA by setting FPD low then LPD low
 */
inline void Hardware_Disable_FPGA(void) {
    // power down seq:  FPD off then LPD off
    FPD_EN_SetLow(); 
    LPD_EN_SetLow();
}

/*
 * To enable FPGA by setting LPD high then FPD high
 */
inline void Hardware_Enable_FPGA(void) {
    // power up seq: LPD on then FPD on
    LPD_EN_SetHigh();
    FPD_EN_SetHigh();
}

/*
 * To turn off Bycon
 */
inline void Hardware_Disable_Bycon(void) {
    BYCON_EN_SetLow();
}

/*
 * turn on Bycon
 */
inline void Hardware_Enable_Bycon(void) {
    BYCON_EN_SetHigh();
}

inline void Hardware_Start_Timer0(uint16_t cycle) {
    TMR0_WriteTimer(cycle);
    // Clearing IF flag
    INTCONbits.TMR0IF=0;
    TMR0_StartTimer();
    Set_Event(EVENT_TIMER0_ON);
}

inline bool Hardware_Timer0_Started(void) {
    return T0CONbits.TMR0ON == 1;
}

inline void Hardware_Stop_Timer0(void) {
    TMR0_StopTimer();
}

inline bool Hardware_Timer0_Overflowed(void) {
    return TMR0_HasOverflowOccured();
}

inline bool Hardware_FPGA_Sleep_Request(void) {
    return FPGA_SLEEP_IN_GetValue();
}

/* Timer1, with help of ECCP, is set to run at 1Hz for Real Time Clock
 */
inline void Hardware_Start_Timer1(void) {
    // Clearing IF flag.
    PIR1bits.TMR1IF = 0;
    TMR1_StartTimer();
}

inline void Hardware_Reset_Timer1(void) {
    TMR1_WriteTimer(0x0000);
    // Clearing IF flag.
    PIR1bits.TMR1IF = 0;
}

inline bool Hardware_ECCP_CompareCompleted(void) {
    return ECCP1_IsCompareComplete();
}

inline void Hardware_ECCP_ClearFlag(void) {
    // Clearing IF flag.
    PIR3bits.CCP1IF = 0;
}


