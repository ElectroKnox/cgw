#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# StandAlone configuration
CND_ARTIFACT_DIR_StandAlone=dist/StandAlone/production
CND_ARTIFACT_NAME_StandAlone=cgw.production.hex
CND_ARTIFACT_PATH_StandAlone=dist/StandAlone/production/cgw.production.hex
CND_PACKAGE_DIR_StandAlone=${CND_DISTDIR}/StandAlone/package
CND_PACKAGE_NAME_StandAlone=cgw.tar
CND_PACKAGE_PATH_StandAlone=${CND_DISTDIR}/StandAlone/package/cgw.tar
# Offset configuration
CND_ARTIFACT_DIR_Offset=dist/Offset/production
CND_ARTIFACT_NAME_Offset=cgw.production.hex
CND_ARTIFACT_PATH_Offset=dist/Offset/production/cgw.production.hex
CND_PACKAGE_DIR_Offset=${CND_DISTDIR}/Offset/package
CND_PACKAGE_NAME_Offset=cgw.tar
CND_PACKAGE_PATH_Offset=${CND_DISTDIR}/Offset/package/cgw.tar
# Combined configuration
CND_ARTIFACT_DIR_Combined=dist/Combined/production
CND_ARTIFACT_NAME_Combined=cgw.production.hex
CND_ARTIFACT_PATH_Combined=dist/Combined/production/cgw.production.hex
CND_PACKAGE_DIR_Combined=${CND_DISTDIR}/Combined/package
CND_PACKAGE_NAME_Combined=cgw.tar
CND_PACKAGE_PATH_Combined=${CND_DISTDIR}/Combined/package/cgw.tar
