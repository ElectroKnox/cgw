/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC18F26K80
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set AN0_12V_P1 aliases
#define AN0_12V_P1_TRIS                 TRISAbits.TRISA0
#define AN0_12V_P1_LAT                  LATAbits.LATA0
#define AN0_12V_P1_PORT                 PORTAbits.RA0
#define AN0_12V_P1_ANS                  ANCON0bits.ANSEL0
#define AN0_12V_P1_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define AN0_12V_P1_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define AN0_12V_P1_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define AN0_12V_P1_GetValue()           PORTAbits.RA0
#define AN0_12V_P1_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define AN0_12V_P1_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define AN0_12V_P1_SetAnalogMode()      do { ANCON0bits.ANSEL0 = 1; } while(0)
#define AN0_12V_P1_SetDigitalMode()     do { ANCON0bits.ANSEL0 = 0; } while(0)

// get/set AN1_12V_P2 aliases
#define AN1_12V_P2_TRIS                 TRISAbits.TRISA1
#define AN1_12V_P2_LAT                  LATAbits.LATA1
#define AN1_12V_P2_PORT                 PORTAbits.RA1
#define AN1_12V_P2_ANS                  ANCON0bits.ANSEL1
#define AN1_12V_P2_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define AN1_12V_P2_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define AN1_12V_P2_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define AN1_12V_P2_GetValue()           PORTAbits.RA1
#define AN1_12V_P2_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define AN1_12V_P2_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define AN1_12V_P2_SetAnalogMode()      do { ANCON0bits.ANSEL1 = 1; } while(0)
#define AN1_12V_P2_SetDigitalMode()     do { ANCON0bits.ANSEL1 = 0; } while(0)

// get/set LPD_EN aliases
#define LPD_EN_TRIS                 TRISAbits.TRISA2
#define LPD_EN_LAT                  LATAbits.LATA2
#define LPD_EN_PORT                 PORTAbits.RA2
#define LPD_EN_ANS                  ANCON0bits.ANSEL2
#define LPD_EN_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define LPD_EN_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define LPD_EN_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define LPD_EN_GetValue()           PORTAbits.RA2
#define LPD_EN_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define LPD_EN_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define LPD_EN_SetAnalogMode()      do { ANCON0bits.ANSEL2 = 1; } while(0)
#define LPD_EN_SetDigitalMode()     do { ANCON0bits.ANSEL2 = 0; } while(0)

// get/set BYCON_EN aliases
#define BYCON_EN_TRIS                 TRISAbits.TRISA3
#define BYCON_EN_LAT                  LATAbits.LATA3
#define BYCON_EN_PORT                 PORTAbits.RA3
#define BYCON_EN_ANS                  ANCON0bits.ANSEL3
#define BYCON_EN_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define BYCON_EN_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define BYCON_EN_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define BYCON_EN_GetValue()           PORTAbits.RA3
#define BYCON_EN_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define BYCON_EN_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define BYCON_EN_SetAnalogMode()      do { ANCON0bits.ANSEL3 = 1; } while(0)
#define BYCON_EN_SetDigitalMode()     do { ANCON0bits.ANSEL3 = 0; } while(0)

// get/set FPGA_SLEEP_IN aliases
#define FPGA_SLEEP_IN_TRIS                 TRISAbits.TRISA5
#define FPGA_SLEEP_IN_LAT                  LATAbits.LATA5
#define FPGA_SLEEP_IN_PORT                 PORTAbits.RA5
#define FPGA_SLEEP_IN_ANS                  ANCON0bits.ANSEL4
#define FPGA_SLEEP_IN_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define FPGA_SLEEP_IN_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define FPGA_SLEEP_IN_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define FPGA_SLEEP_IN_GetValue()           PORTAbits.RA5
#define FPGA_SLEEP_IN_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define FPGA_SLEEP_IN_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define FPGA_SLEEP_IN_SetAnalogMode()      do { ANCON0bits.ANSEL4 = 1; } while(0)
#define FPGA_SLEEP_IN_SetDigitalMode()     do { ANCON0bits.ANSEL4 = 0; } while(0)

// get/set TEST1 aliases
#define TEST1_TRIS                 TRISAbits.TRISA6
#define TEST1_LAT                  LATAbits.LATA6
#define TEST1_PORT                 PORTAbits.RA6
#define TEST1_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define TEST1_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define TEST1_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define TEST1_GetValue()           PORTAbits.RA6
#define TEST1_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define TEST1_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)

// get/set TEST2 aliases
#define TEST2_TRIS                 TRISAbits.TRISA7
#define TEST2_LAT                  LATAbits.LATA7
#define TEST2_PORT                 PORTAbits.RA7
#define TEST2_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define TEST2_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define TEST2_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define TEST2_GetValue()           PORTAbits.RA7
#define TEST2_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define TEST2_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)

// get/set LIN_IRQN_IN aliases
#define LIN_IRQN_IN_TRIS                 TRISBbits.TRISB0
#define LIN_IRQN_IN_LAT                  LATBbits.LATB0
#define LIN_IRQN_IN_PORT                 PORTBbits.RB0
#define LIN_IRQN_IN_WPU                  WPUBbits.WPUB0
#define LIN_IRQN_IN_ANS                  ANCON1bits.ANSEL10
#define LIN_IRQN_IN_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define LIN_IRQN_IN_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define LIN_IRQN_IN_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define LIN_IRQN_IN_GetValue()           PORTBbits.RB0
#define LIN_IRQN_IN_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define LIN_IRQN_IN_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define LIN_IRQN_IN_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define LIN_IRQN_IN_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define LIN_IRQN_IN_SetAnalogMode()      do { ANCON1bits.ANSEL10 = 1; } while(0)
#define LIN_IRQN_IN_SetDigitalMode()     do { ANCON1bits.ANSEL10 = 0; } while(0)

// get/set CAN_IRQN_IN aliases
#define CAN_IRQN_IN_TRIS                 TRISBbits.TRISB1
#define CAN_IRQN_IN_LAT                  LATBbits.LATB1
#define CAN_IRQN_IN_PORT                 PORTBbits.RB1
#define CAN_IRQN_IN_WPU                  WPUBbits.WPUB1
#define CAN_IRQN_IN_ANS                  ANCON1bits.ANSEL8
#define CAN_IRQN_IN_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define CAN_IRQN_IN_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define CAN_IRQN_IN_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define CAN_IRQN_IN_GetValue()           PORTBbits.RB1
#define CAN_IRQN_IN_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define CAN_IRQN_IN_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define CAN_IRQN_IN_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define CAN_IRQN_IN_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define CAN_IRQN_IN_SetAnalogMode()      do { ANCON1bits.ANSEL8 = 1; } while(0)
#define CAN_IRQN_IN_SetDigitalMode()     do { ANCON1bits.ANSEL8 = 0; } while(0)

// get/set RB2 procedures
#define RB2_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define RB2_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define RB2_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define RB2_GetValue()              PORTBbits.RB2
#define RB2_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define RB2_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define RB2_SetPullup()             do { WPUBbits.WPUB2 = 1; } while(0)
#define RB2_ResetPullup()           do { WPUBbits.WPUB2 = 0; } while(0)

// get/set RB3 procedures
#define RB3_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define RB3_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define RB3_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define RB3_GetValue()              PORTBbits.RB3
#define RB3_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define RB3_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define RB3_SetPullup()             do { WPUBbits.WPUB3 = 1; } while(0)
#define RB3_ResetPullup()           do { WPUBbits.WPUB3 = 0; } while(0)

// get/set FPD_EN aliases
#define FPD_EN_TRIS                 TRISBbits.TRISB4
#define FPD_EN_LAT                  LATBbits.LATB4
#define FPD_EN_PORT                 PORTBbits.RB4
#define FPD_EN_WPU                  WPUBbits.WPUB4
#define FPD_EN_ANS                  ANCON1bits.ANSEL9
#define FPD_EN_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define FPD_EN_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define FPD_EN_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define FPD_EN_GetValue()           PORTBbits.RB4
#define FPD_EN_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define FPD_EN_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define FPD_EN_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define FPD_EN_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define FPD_EN_SetAnalogMode()      do { ANCON1bits.ANSEL9 = 1; } while(0)
#define FPD_EN_SetDigitalMode()     do { ANCON1bits.ANSEL9 = 0; } while(0)

// get/set TEST3 aliases
#define TEST3_TRIS                 TRISBbits.TRISB5
#define TEST3_LAT                  LATBbits.LATB5
#define TEST3_PORT                 PORTBbits.RB5
#define TEST3_WPU                  WPUBbits.WPUB5
#define TEST3_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define TEST3_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define TEST3_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define TEST3_GetValue()           PORTBbits.RB5
#define TEST3_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define TEST3_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define TEST3_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define TEST3_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)

// get/set PIC_ICSPCLK aliases
#define PIC_ICSPCLK_TRIS                 TRISBbits.TRISB6
#define PIC_ICSPCLK_LAT                  LATBbits.LATB6
#define PIC_ICSPCLK_PORT                 PORTBbits.RB6
#define PIC_ICSPCLK_WPU                  WPUBbits.WPUB6
#define PIC_ICSPCLK_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define PIC_ICSPCLK_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define PIC_ICSPCLK_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define PIC_ICSPCLK_GetValue()           PORTBbits.RB6
#define PIC_ICSPCLK_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define PIC_ICSPCLK_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define PIC_ICSPCLK_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define PIC_ICSPCLK_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)

// get/set PIC_ICSPDAT aliases
#define PIC_ICSPDAT_TRIS                 TRISBbits.TRISB7
#define PIC_ICSPDAT_LAT                  LATBbits.LATB7
#define PIC_ICSPDAT_PORT                 PORTBbits.RB7
#define PIC_ICSPDAT_WPU                  WPUBbits.WPUB7
#define PIC_ICSPDAT_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define PIC_ICSPDAT_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define PIC_ICSPDAT_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define PIC_ICSPDAT_GetValue()           PORTBbits.RB7
#define PIC_ICSPDAT_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define PIC_ICSPDAT_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define PIC_ICSPDAT_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define PIC_ICSPDAT_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)

// get/set OBDII_IRQN_IN aliases
#define OBDII_IRQN_IN_TRIS                 TRISCbits.TRISC2
#define OBDII_IRQN_IN_LAT                  LATCbits.LATC2
#define OBDII_IRQN_IN_PORT                 PORTCbits.RC2
#define OBDII_IRQN_IN_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define OBDII_IRQN_IN_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define OBDII_IRQN_IN_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define OBDII_IRQN_IN_GetValue()           PORTCbits.RC2
#define OBDII_IRQN_IN_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define OBDII_IRQN_IN_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)

// get/set I2C_SCL aliases
#define I2C_SCL_TRIS                 TRISCbits.TRISC3
#define I2C_SCL_LAT                  LATCbits.LATC3
#define I2C_SCL_PORT                 PORTCbits.RC3
#define I2C_SCL_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define I2C_SCL_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define I2C_SCL_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define I2C_SCL_GetValue()           PORTCbits.RC3
#define I2C_SCL_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define I2C_SCL_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)

// get/set I2C_SDA aliases
#define I2C_SDA_TRIS                 TRISCbits.TRISC4
#define I2C_SDA_LAT                  LATCbits.LATC4
#define I2C_SDA_PORT                 PORTCbits.RC4
#define I2C_SDA_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define I2C_SDA_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define I2C_SDA_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define I2C_SDA_GetValue()           PORTCbits.RC4
#define I2C_SDA_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define I2C_SDA_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)

// get/set CAN0_STBY_N aliases
#define CAN0_STBY_N_TRIS                 TRISCbits.TRISC5
#define CAN0_STBY_N_LAT                  LATCbits.LATC5
#define CAN0_STBY_N_PORT                 PORTCbits.RC5
#define CAN0_STBY_N_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define CAN0_STBY_N_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define CAN0_STBY_N_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define CAN0_STBY_N_GetValue()           PORTCbits.RC5
#define CAN0_STBY_N_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define CAN0_STBY_N_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)

// get/set BL_IRQ_OUT aliases
#define BL_IRQ_OUT_TRIS                 TRISCbits.TRISC6
#define BL_IRQ_OUT_LAT                  LATCbits.LATC6
#define BL_IRQ_OUT_PORT                 PORTCbits.RC6
#define BL_IRQ_OUT_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define BL_IRQ_OUT_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define BL_IRQ_OUT_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define BL_IRQ_OUT_GetValue()           PORTCbits.RC6
#define BL_IRQ_OUT_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define BL_IRQ_OUT_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)

// get/set BL_EN_IN aliases
#define BL_EN_IN_TRIS                 TRISCbits.TRISC7
#define BL_EN_IN_LAT                  LATCbits.LATC7
#define BL_EN_IN_PORT                 PORTCbits.RC7
#define BL_EN_IN_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define BL_EN_IN_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define BL_EN_IN_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define BL_EN_IN_GetValue()           PORTCbits.RC7
#define BL_EN_IN_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define BL_EN_IN_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/
