/*
 * File:   app_config.c
 * Author:
 *
 */

#include "app_config.h"

const uint32_t _idCode         __at(0xFFF0) = ID_CODE;
const uint16_t _idVersionMajor __at(0xFFF4) = ID_VERSION_MAJOR;
const uint16_t _idVersionMinor __at(0xFFF6) = ID_VERSION_MINOR;

const uint16_t _canRxId[2] = {CAN_RX_BYCON_STDID, CAN_RX_FULLPWR_ON_STDID};

static EVENT event = 0;
static uint8_t wakeupSource = 0;
/* Unix Epoch Clock Timestamp in seconds */
static long long realTimeClock = TIMESTAMP;

void Clear_Event(EVENT e) {
    event &= ~e;
}

void Set_Event(EVENT e) {
    event |= e;
}

bool Event_Exists(EVENT e) {
    return event & e;
}

uint8_t Get_Wakeup_Source(void) {
    return wakeupSource;
}

void Clear_Wakeup_Source(void) {
    wakeupSource = 0;
}

void Set_Wakeup_Source(WAKEUP_SOURCE source) {
    wakeupSource |= source;
}

inline void Set_RealTimeClock(long long time) {
    realTimeClock = time;
}

inline void Update_RealTimeClock(void) {
    ++realTimeClock;
}

inline long long Get_RealTimeClock(void) {
    return realTimeClock;
}
