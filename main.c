/*
 * File:   main.c
 * Author: 
 *
 */

#include <stdint.h>
#include <stdbool.h>
#include "can_comm.h"
#include "app_config.h"
#include "hardware.h"
#include "usart_comm.h"

#ifdef __DEBUG
#define ETX_DEBUG
#endif

static STATE state;
static long long fpgaSleepTime = 0;

void BootComplete_Check(void);
void Enter_Idle_CAN_Trx_Off_Mode(void);
void Enter_Idle_CAN_Trx_On_Mode(void);
void Enter_Bycon_Active_Mode(void);
void Enter_Full_Power_Mode(void);
void Update_Events(void);
void Update_State(void);
void Update_USART_Comm(void);
void Update_Real_Time_Clock(void);

void BootComplete_Check(void) {
    USART_Comm_TX_Bootcomplete();
    if (RCONbits.CM == 0) { // Device configuration mismatch occurred
       RCONbits.CM = 1; 
    }
    if (RCONbits.RI == 0) { // RESET instruction executed
       RCONbits.RI = 1; 
    }
    if (RCONbits.POR == 0) { // Power-On Reset occurred
       RCONbits.POR = 1; 
    }
    if (RCONbits.BOR == 0) { // Brown-out Reset occurred
       RCONbits.BOR = 1; 
    }
}

//------------------------------------------------------------
void Enter_Idle_CAN_Trx_Off_Mode(void) {
    Hardware_Disable_Bycon();
    Hardware_Disable_FPGA();
    Hardware_Disable_CAN_Transceiver(); //to save power
    Hardware_Take_CAN_Transceiver();
    Hardware_Take_CAN_Bus();
}

//------------------------------------------------------------
void Enter_Idle_CAN_Trx_On_Mode(void) {
    Hardware_Disable_Bycon();
    Hardware_Disable_FPGA();
    Hardware_Enable_CAN_Transceiver();
    Hardware_Take_CAN_Transceiver();
    Hardware_Take_CAN_Bus();
}

//------------------------------------------------------------
void Enter_Bycon_Active_Mode(void) {
    Hardware_Disable_FPGA();
    Hardware_Enable_Bycon();
    Hardware_Enable_CAN_Transceiver();
    Hardware_Take_CAN_Transceiver();
    Hardware_Take_CAN_Bus();
}

//------------------------------------------------------------
void Enter_Full_Power_Mode(void) {
    Hardware_Enable_CAN_Transceiver();
    Hardware_Give_CAN_Transceiver();
    Hardware_Give_CAN_Bus();
    Hardware_Enable_FPGA();
    Hardware_Enable_Bycon();
}

void Update_Events(void) {
    if (state == STATE_FULL_POWER) {
        if (Hardware_FPGA_Sleep_Request()) {
            Set_Event(EVENT_FPGA_SLEEP_REQUEST);
            if (!fpgaSleepTime)
                fpgaSleepTime = Get_RealTimeClock();
        }
    }
    
    if (CAN0_RX_FLAG_SET) {
        CLEAR_CAN0_RX_FLAG();
        Set_Event(EVENT_CAN0_RX_FLAG);
    }

    if (!CAN_Comm_Buffer_Empty()) {
        CAN_Comm_Handle_RX_Message();
    }

    if (state != STATE_FULL_POWER) {
        if (LIN_IRQ_FLAG_SET) {
            CLEAR_LIN_IRQ_FLAG();
            Set_Event(EVENT_FULL_POWER_REQUEST);
            Set_Wakeup_Source(WAKEUP_SOURCE_LIN_IRQ);
        }
        if (CAN_IRQ_FLAG_SET) {
            CLEAR_CAN_IRQ_FLAG();
            Set_Event(EVENT_FULL_POWER_REQUEST);
            Set_Wakeup_Source(WAKEUP_SOURCE_CAN_IRQ);
        }
    }
}

void Update_State(void) {
    switch (state) {
        case STATE_IDLE_CAN_TRX_ON:
            if (Event_Exists(EVENT_FULL_POWER_REQUEST)) {
                Clear_Event(EVENT_FULL_POWER_REQUEST | EVENT_BYCON_ON_REQUEST | EVENT_BYCON_OFF_REQUEST);
                Enter_Full_Power_Mode();
                if (Event_Exists(EVENT_TIMER0_ON)) {
                    Clear_Event(EVENT_TIMER0_ON);
                    Hardware_Stop_Timer0();
                }
                state = STATE_FULL_POWER;
                break;
            }
            if (Event_Exists(EVENT_BYCON_ON_REQUEST)) {
                Clear_Event(EVENT_BYCON_ON_REQUEST | EVENT_BYCON_OFF_REQUEST);
                Enter_Bycon_Active_Mode();
                if (Event_Exists(EVENT_TIMER0_ON)) {
                    Clear_Event(EVENT_TIMER0_ON);
                    Hardware_Stop_Timer0();
                }
                state = STATE_BYCON_ACTIVE;
                break;
            }
            if (Event_Exists(EVENT_CAN0_RX_FLAG)) {
                Clear_Event(EVENT_CAN0_RX_FLAG);
                // Restart timer for CAN_TRX_OFF count down
                Hardware_Start_Timer0(TIMER0_CYCLE_500_MILLI_SECOND);
            }
            if (Event_Exists(EVENT_TIMER0_ON)) {
                if (Hardware_Timer0_Overflowed()) {
                    Hardware_Stop_Timer0();
                    Clear_Event(EVENT_TIMER0_ON);
                    state = STATE_IDLE_CAN_TRX_OFF;
                }
            }
            break;
        case STATE_IDLE_CAN_TRX_OFF:
            if (Event_Exists(EVENT_FULL_POWER_REQUEST)) {
                Clear_Event(EVENT_FULL_POWER_REQUEST | EVENT_BYCON_ON_REQUEST | EVENT_BYCON_OFF_REQUEST);
                Enter_Full_Power_Mode();
                state = STATE_FULL_POWER;
                break;
            }
            if (Event_Exists(EVENT_BYCON_ON_REQUEST)) {
                Clear_Event(EVENT_BYCON_ON_REQUEST | EVENT_BYCON_OFF_REQUEST);
                Enter_Bycon_Active_Mode();
                state = STATE_BYCON_ACTIVE;
                break;
            }
            if (Event_Exists(EVENT_CAN0_RX_FLAG)) {
                Clear_Event(EVENT_CAN0_RX_FLAG);
                Enter_Idle_CAN_Trx_On_Mode();
                Hardware_Start_Timer0(TIMER0_CYCLE_500_MILLI_SECOND);
                state = STATE_IDLE_CAN_TRX_ON;
            }
            break;
        case STATE_BYCON_ACTIVE:
            if (Event_Exists(EVENT_FULL_POWER_REQUEST)) {
                Clear_Event(EVENT_FULL_POWER_REQUEST | EVENT_BYCON_ON_REQUEST | EVENT_BYCON_OFF_REQUEST);
                Enter_Full_Power_Mode();
                state = STATE_FULL_POWER;
                break;
            }
            if (Event_Exists(EVENT_BYCON_OFF_REQUEST)) {
                Clear_Event(EVENT_BYCON_OFF_REQUEST | EVENT_CAN0_RX_FLAG);
                Enter_Idle_CAN_Trx_Off_Mode();
                Hardware_Start_Timer0(TIMER0_CYCLE_500_MILLI_SECOND);
                state = STATE_BYCON_SLEEP_PENDING;
                break;
            }
            break;
        case STATE_FULL_POWER:
        {
            long long currentTime = Get_RealTimeClock();
            if (Event_Exists(EVENT_FPGA_SLEEP_REQUEST) && (currentTime > fpgaSleepTime + 5)) {
                Clear_Event(EVENT_FPGA_SLEEP_REQUEST | EVENT_CAN0_RX_FLAG);
                Enter_Idle_CAN_Trx_Off_Mode();
                Hardware_Start_Timer0(TIMER0_CYCLE_1_SECOND);
                state = STATE_FPGA_SLEEP_PENDING;
            }
            break;
        }
        case STATE_FPGA_SLEEP_PENDING:
            if (Event_Exists(EVENT_TIMER0_ON)) {
                if (Hardware_Timer0_Overflowed()) {
                    //Make sure the FPGA_Sleep is low
                    if (!Hardware_FPGA_Sleep_Request()) {
                        Hardware_Stop_Timer0();
                        Clear_Event(EVENT_TIMER0_ON);
                        state = STATE_IDLE_CAN_TRX_OFF;
                        // Clear flags that may be triggered when FPGA was active
                        CLEAR_LIN_IRQ_FLAG();
                        CLEAR_CAN_IRQ_FLAG();
                        Clear_Wakeup_Source();
                        Clear_Event(EVENT_FULL_POWER_REQUEST);
                    }
                    else {
                        // If not, reset timer for another 500ms
                        Hardware_Start_Timer0(TIMER0_CYCLE_500_MILLI_SECOND);
                    }
                }
            }
            break;
        case STATE_BYCON_SLEEP_PENDING:
            if (Event_Exists(EVENT_TIMER0_ON)) {
                if (Hardware_Timer0_Overflowed()) {
                    Hardware_Stop_Timer0();
                    Clear_Event(EVENT_TIMER0_ON);
                    state = STATE_IDLE_CAN_TRX_OFF;
                }
            }
            break;
        default:
            break;
    }
}

void Update_USART_Comm(void) {
    if (state == STATE_FULL_POWER) {
        if (Event_Exists(EVENT_USART_TX_START)) {
            if (USART_Comm_Handle_TX_Message() == true) {
                Clear_Event(EVENT_USART_TX_START);
            }
        }
        USART_Comm_Handle_RX_Message();
    }
}

void Update_Real_Time_Clock(void) {
    if (Hardware_ECCP_CompareCompleted()) {
        Hardware_ECCP_ClearFlag();
        Update_RealTimeClock();
    }
}

void main(void) {
    Hardware_Disable_Watchdog();
    Hardware_Disable_Interrupt_Handler();
    Hardware_Init();
    Hardware_Start_Timer1();
#ifndef __DEBUG
    Hardware_Enable_Watchdog();
#endif
    BootComplete_Check();

    Enter_Full_Power_Mode();
    state = STATE_FULL_POWER;
    
    Set_RealTimeClock(1);
    while (1) {
        Update_Events();
        Update_State();
        Update_USART_Comm();
        Update_Real_Time_Clock();
#ifndef __DEBUG
        Hardware_Clear_Watchdog();
#endif
    }
}

/**
 End of File
 */
