/*
 * File:   can_comm.h
 * Author: 
 *
 */

#ifndef CAN_COMM_H
#define	CAN_COMM_H

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#include <xc.h>
#include <stdbool.h>

bool CAN_Comm_Buffer_Empty(void);

void CAN_Comm_Handle_RX_Message(void);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* CAN_COMM_H */

