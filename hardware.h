/*
 * File:   hardware.h
 * Author:
 *
 */

#ifndef HARDWARE_H
#define	HARDWARE_H

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include "mcc_generated_files/ext_int.h"

#define LIN_IRQ_FLAG_SET (INTCONbits.INT0IF == 1)
#define CAN_IRQ_FLAG_SET (INTCON3bits.INT1IF == 1)
#define CAN0_RX_FLAG_SET (INTCON3bits.INT3IF == 1)

#define CLEAR_LIN_IRQ_FLAG() (INTCONbits.INT0IF = 0)
#define CLEAR_CAN_IRQ_FLAG() (INTCON3bits.INT1IF = 0)
#define CLEAR_CAN0_RX_FLAG() (INTCON3bits.INT3IF = 0)

/*
 * Timer0 cycle settings for TMR0H and TMR0L. 
 * Value may change if clock setting changes.
 */
#define TIMER0_CYCLE_100_MILLI_SECOND   0xF9E5
#define TIMER0_CYCLE_500_MILLI_SECOND   0xE17B
#define TIMER0_CYCLE_1_SECOND           0xC2F6

inline void Hardware_Init(void);

inline void Hardware_Disable_Interrupt_Handler(void);

inline void Hardware_Disable_Watchdog(void);

inline void Hardware_Enable_Watchdog(void);

inline void Hardware_Clear_Watchdog(void);

inline void Hardware_Disable_CAN_Transceiver(void);

inline void Hardware_Enable_CAN_Transceiver(void);

inline void Hardware_Give_CAN_Transceiver(void);

inline void Hardware_Take_CAN_Transceiver(void);

inline void Hardware_Give_CAN_Bus(void);

inline void Hardware_Take_CAN_Bus(void);

inline void Hardware_Disable_FPGA(void);

inline void Hardware_Enable_FPGA(void);

inline void Hardware_Disable_Bycon(void);

inline void Hardware_Enable_Bycon(void);

inline void Hardware_Start_Timer0(uint16_t cycle);

inline void Hardware_Stop_Timer0(void);

inline bool Hardware_Timer0_Overflowed(void);

inline bool Hardware_FPGA_Sleep_Request(void);

inline void Hardware_Start_Timer1(void);

inline void Hardware_Reset_Timer1(void);

inline bool Hardware_ECCP_CompareCompleted(void);

inline void Hardware_ECCP_ClearFlag(void);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* HARDWARE_H */

