/*
 * File:   usart_comm.c
 * Author: 
 *
 */

#include <xc.h>
#include "app_config.h"
#include "usart_comm.h"
#include "hardware.h"
#include "mcc_generated_files/eusart2.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/adc.h"

static USART_FRAME rxUsartMessage;
static USART_STATE rxState = USART_STATE_HEADER;
static uint8_t rxIndex = 0;
static uint8_t rxLength = 0;

static USART_FRAME txUsartMessage;
static USART_STATE txState = USART_STATE_START;
static uint8_t txIndex = 0;
static uint8_t txLength = 0;

/*
 * PIC USART AUTOBUAD requires master to initiate communication first. We cannot
 * TX until we receive the Auto Baud 0x55 from the master, which sets
 * autoBaudReceived to true.
 */
static bool autoBaudReceived = false;

void USART_Comm_Handle_RX_Message(void) {
    switch(rxState) {
        case USART_STATE_HEADER:
            if (EUSART2_is_rx_ready()) {
                uint8_t ch;
                ch = EUSART2_Read();
                if (ch == USART_FRAME_HEADER) {
                    autoBaudReceived = true;
                    rxIndex = 0;
                    rxUsartMessage.buffer[rxIndex++] = ch;
                    rxState = USART_STATE_LENGTH;
                }
            }
            break;
        case USART_STATE_LENGTH:
            if (EUSART2_is_rx_ready()) {
                rxUsartMessage.buffer[rxIndex++] = EUSART2_Read();
                rxLength = rxUsartMessage.length + sizeof(rxUsartMessage.prefix);
                rxState = USART_STATE_COMMANDID;
            }
            break;
        case USART_STATE_COMMANDID:
        case USART_STATE_PAYLOAD:
            if (EUSART2_is_rx_ready()) {
                rxUsartMessage.buffer[rxIndex++] = EUSART2_Read();
                if (rxLength == rxIndex) {
                    if (Event_Exists(EVENT_USART_TX_START)) {
                        rxState = USART_STATE_OVERRUN;
                    }
                    else {
                        rxState = USART_STATE_HEADER;
                        USART_Comm_Process_RX_Message();
                    }
                }
            }
            break;
        case USART_STATE_OVERRUN:
            if (!Event_Exists(EVENT_USART_TX_START)) {
                rxState = USART_STATE_HEADER;
                USART_Comm_Process_RX_Message();
            }
        default:
            break;
    }
}

void USART_Comm_Process_RX_Message(void) {
    switch(rxUsartMessage.commandId) {
        case USART_COMMAND_REAL_TIME_CLOCK:
            if (rxUsartMessage.length == sizeof(rxUsartMessage.realTimeClock) - sizeof(rxUsartMessage.prefix)) {
                // Has payload, this is a Set Request
                Set_RealTimeClock(rxUsartMessage.realTimeClock.timeStamp);
                Hardware_Reset_Timer1();
            }
            USART_Comm_TX_Message_Init(rxUsartMessage.commandId);
            Set_Event(EVENT_USART_TX_START);
            break;
        case USART_COMMAND_WAKEUP_SOURCE:
            if (rxUsartMessage.length == sizeof(rxUsartMessage.wakeupSource) - sizeof(rxUsartMessage.prefix)) {
                // Has payload, this is a Clear Request
                Clear_Wakeup_Source();
            }
            USART_Comm_TX_Message_Init(rxUsartMessage.commandId);
            Set_Event(EVENT_USART_TX_START);
            break;
        default:
            USART_Comm_TX_Message_Init(rxUsartMessage.commandId);
            Set_Event(EVENT_USART_TX_START);
            break;
    }
}

void USART_Comm_TX_Message_Init_Status(void) {
    long long currentTime = Get_RealTimeClock();
    uint16_t adc0, adc1;
    adc0 = ADC_GetConversion(channel_AN0);
    adc1 = ADC_GetConversion(channel_AN1);
    
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.status) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_STATUS;
    // Device ID
    txUsartMessage.status.idCode = ID_CODE;
    // Firmware version
    txUsartMessage.status.idVersionMajor = ID_VERSION_MAJOR;
    txUsartMessage.status.idVersionMinor = ID_VERSION_MINOR;
    // Real time clock
    txUsartMessage.status.timeStamp = currentTime;
    // ADC
    txUsartMessage.status.adc0 = adc0;
    txUsartMessage.status.adc1 = adc1;
    // ODBII Status
    txUsartMessage.status.obdii = OBDII_IRQN_IN_GetValue();
    // Wakeup Source
    txUsartMessage.status.wakeupSource = Get_Wakeup_Source();
}

void USART_Comm_TX_Message_Init_Firmware_Version(void) {
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.firmwareVersion) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_FIRMWARE_VERSION;
    txUsartMessage.firmwareVersion.idVersionMajor = ID_VERSION_MAJOR;
    txUsartMessage.firmwareVersion.idVersionMinor = ID_VERSION_MINOR;
}

void USART_Comm_TX_Message_Init_Real_Time_Clock(void) {
    long long currentTime = Get_RealTimeClock();
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.realTimeClock) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_REAL_TIME_CLOCK;
    txUsartMessage.realTimeClock.timeStamp = currentTime;
}

void USART_Comm_TX_Message_Init_Voltage_Status(void) {
    /* One ADC conversion takes about 35us in current clock settings
     */
    uint16_t adc0, adc1;
    adc0 = ADC_GetConversion(channel_AN0);
    adc1 = ADC_GetConversion(channel_AN1);
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.voltageStatus) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_VOLTAGE_STATUS;
    txUsartMessage.voltageStatus.adc0 = adc0;
    txUsartMessage.voltageStatus.adc1 = adc1;
}

void USART_Comm_TX_Message_Init_OBDII_Status(void) {
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.obdiiStatus) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_OBDII_STATUS;
    txUsartMessage.obdiiStatus.obdii = OBDII_IRQN_IN_GetValue();
}

void USART_Comm_TX_Message_Init_Wakeup_Source(void) {
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.wakeupSource) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_WAKEUP_SOURCE;
    txUsartMessage.wakeupSource.source = Get_Wakeup_Source();
}

void USART_Comm_TX_Message_Init_Bootcomplete(void) {
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.bootComplete) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_BOOTCOMPLETE;
    txUsartMessage.bootComplete.rcon = RCON;   // RESET CONTROL REGISTER
    txUsartMessage.bootComplete.stkptr = STKPTR; // STACK POINTER REGISTER
    txUsartMessage.bootComplete.reserved = 0x0000;   // Reserved
}

void USART_Comm_TX_Message_Init_Undefined(void) {
    txUsartMessage.header = USART_FRAME_HEADER;
    txUsartMessage.length = sizeof(txUsartMessage.undefined) - sizeof(txUsartMessage.prefix);
    txUsartMessage.commandId = USART_COMMAND_UNDEFINED;
}

void USART_Comm_TX_Message_Init(USART_COMMAND command) {
    switch(command) {
        case USART_COMMAND_STATUS:
            USART_Comm_TX_Message_Init_Status();
            break;
        case USART_COMMAND_FIRMWARE_VERSION:
            USART_Comm_TX_Message_Init_Firmware_Version();
            break;
        case USART_COMMAND_REAL_TIME_CLOCK:
            USART_Comm_TX_Message_Init_Real_Time_Clock();
            break;
        case USART_COMMAND_VOLTAGE_STATUS:
            USART_Comm_TX_Message_Init_Voltage_Status();
            break;
        case USART_COMMAND_OBDII_STATUS:
            USART_Comm_TX_Message_Init_OBDII_Status();
            break;
        case USART_COMMAND_WAKEUP_SOURCE:
            USART_Comm_TX_Message_Init_Wakeup_Source();
            break;
        case USART_COMMAND_BOOTCOMPLETE:
            USART_Comm_TX_Message_Init_Bootcomplete();
            break;
        default:
            USART_Comm_TX_Message_Init_Undefined();
            break;
    }
    txLength = txUsartMessage.length + sizeof(txUsartMessage.prefix);
}

bool USART_Comm_Handle_TX_Message(void) {
    if (autoBaudReceived == true) {
        switch(txState) {
            case USART_STATE_START:
                if (EUSART2_is_tx_ready()) {
                    EUSART2_Write(USART_AUTOBAUD_START);
                    txIndex = 0;
                    txState = USART_STATE_HEADER;
                }
                break;
            case USART_STATE_HEADER:
            case USART_STATE_LENGTH:
            case USART_STATE_COMMANDID:
            case USART_STATE_PAYLOAD:
            case USART_STATE_OVERRUN:
                if (EUSART2_is_tx_ready()) {
                    EUSART2_Write(txUsartMessage.buffer[txIndex++]);
                    if (txIndex == txLength) {
                        txState = USART_STATE_START;
                        return true;
                    }
                }
                break;
            default:
                break;
        }
    }
    return false;
}

void USART_Comm_TX_Bootcomplete(void) {
    USART_Comm_TX_Message_Init(USART_COMMAND_BOOTCOMPLETE);
    Set_Event(EVENT_USART_TX_START);
}
