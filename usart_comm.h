/* 
 * File:   usart_comm.h
 * Author: 
 *
 */

#ifndef USART_COMM_H
#define	USART_COMM_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdint.h>
#include <stdbool.h>

#define USART_BUFFER_MAX_SIZE       128
#define USART_PAYLOAD_MAX_SIZE      (USART_BUFFER_MAX_SIZE-3)
#define USART_FRAME_HEADER          0xBB
#define USART_AUTOBAUD_START        0x55

typedef enum {
    USART_COMMAND_STATUS            = 0x00,
    USART_COMMAND_FIRMWARE_VERSION  = 0x01,
    USART_COMMAND_REAL_TIME_CLOCK   = 0x02,
    USART_COMMAND_VOLTAGE_STATUS    = 0x03,
    USART_COMMAND_OBDII_STATUS      = 0x04,
    USART_COMMAND_WAKEUP_SOURCE     = 0x05,
    USART_COMMAND_BOOTCOMPLETE      = 0xFD,
    USART_COMMAND_UNDEFINED         = 0xFE
} USART_COMMAND;

typedef enum {
    USART_STATE_START,
    USART_STATE_HEADER,
    USART_STATE_LENGTH,
    USART_STATE_COMMANDID,
    USART_STATE_PAYLOAD,
    USART_STATE_OVERRUN
} USART_STATE;

#pragma pack(1)
typedef union {
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        uint8_t     payload[USART_PAYLOAD_MAX_SIZE];
    };
    struct {
        uint8_t     header;
        uint8_t     length;
    } prefix;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        uint32_t    idCode;
        uint16_t    idVersionMajor;
        uint16_t    idVersionMinor;
        long long   timeStamp;
        uint16_t    adc0;
        uint16_t    adc1;
        uint8_t     obdii;
        uint8_t     wakeupSource;
    } status;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        uint16_t    idVersionMajor;
        uint16_t    idVersionMinor;
    } firmwareVersion;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        long long   timeStamp;
    } realTimeClock;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        uint16_t    adc0;
        uint16_t    adc1;
    } voltageStatus;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        uint8_t     obdii;
    } obdiiStatus;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        uint8_t     source;
    } wakeupSource;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
        uint8_t     rcon;
        uint8_t     stkptr;
        uint16_t    reserved;
    } bootComplete;
    struct {
        uint8_t     header;
        uint8_t     length;
        uint8_t     commandId;
    } undefined;
    uint8_t buffer[USART_BUFFER_MAX_SIZE];
} USART_FRAME;

void USART_Comm_Handle_RX_Message(void);

void USART_Comm_Process_RX_Message(void);

void USART_Comm_TX_Message_Init_Status(void);

void USART_Comm_TX_Message_Init_Firmware_Version(void);

void USART_Comm_TX_Message_Init_Real_Time_Clock(void);

void USART_Comm_TX_Message_Init_Voltage_Status(void);

void USART_Comm_TX_Message_Init_OBDII_Status(void);

void USART_Comm_TX_Message_Init_Bootcomplete(void);

void USART_Comm_TX_Message_Init_Undefined(void);

void USART_Comm_TX_Message_Init(USART_COMMAND command);

bool USART_Comm_Handle_TX_Message(void);

void USART_Comm_TX_Bootcomplete(void);

#ifdef	__cplusplus
}
#endif

#endif	/* USART_COMM_H */

