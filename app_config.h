/*
 * File:   app_config.h
 * Author:
 *
 */

#ifndef APP_CONFIG_H
#define	APP_CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>
#include <stdint.h>
#include <xc.h>

#define ID_CODE             0x434757
#define ID_VERSION_MAJOR    0x0000
#define ID_VERSION_MINOR    0x0004

#define TIMESTAMP   0x5e50407f

#define CAN_RX_BYCON_STDID      0x2AC
#define CAN_RX_BYCON_DLEN       2
#define CAN_RX_BYCON_DATA0      0x00
#define CAN_RX_BYCON_DATA1_ON   0x10
#define CAN_RX_BYCON_DATA1_OFF  0x00

#define CAN_RX_FULLPWR_ON_STDID 0x55F

typedef enum {
    STATE_IDLE_CAN_TRX_ON       = 0,
    STATE_IDLE_CAN_TRX_OFF      = 1,
    STATE_BYCON_ACTIVE          = 2,
    STATE_FULL_POWER            = 3,
    STATE_FPGA_SLEEP_PENDING    = 4,
    STATE_BYCON_SLEEP_PENDING   = 5,
    STATE_UNDEFINED             = 0xFF
} STATE;

typedef enum {
    EVENT_FPGA_SLEEP_REQUEST    = 0x01 << 0,
    EVENT_FULL_POWER_REQUEST    = 0x01 << 1,
    EVENT_BYCON_ON_REQUEST      = 0x01 << 2,
    EVENT_BYCON_OFF_REQUEST     = 0x01 << 3,
    EVENT_CAN0_RX_FLAG          = 0x01 << 4,
    EVENT_TIMER0_ON             = 0x01 << 5,
    EVENT_USART_TX_START        = 0x01 << 6
} EVENT;

typedef enum {
#ifdef CGW
    WAKEUP_SOURCE_CAN_RX_0X55F  = 0x01 << 0,
    WAKEUP_SOURCE_LIN_IRQ       = 0x01 << 1,
    WAKEUP_SOURCE_CAN_IRQ       = 0x01 << 2
#elif defined ATB
    WAKEUP_SOURCE_CAN_RX_0X1FC  = 0x01 << 0,
    WAKEUP_SOURCE_CAN_RX_0X550  = 0x01 << 1,
    WAKEUP_SOURCE_BLE_FULL_POWER= 0x01 << 2,
    WAKEUP_SOURCE_LTE2_IRQ      = 0x01 << 3,
    WAKEUP_SOURCE_LTE3_IRQ      = 0x01 << 4
#endif
} WAKEUP_SOURCE;

void Clear_Event(EVENT e);
void Set_Event(EVENT e);
bool Event_Exists(EVENT e);
uint8_t Get_Wakeup_Source(void);
void Clear_Wakeup_Source(void);
void Set_Wakeup_Source(WAKEUP_SOURCE source);
inline void Set_RealTimeClock(long long time);
inline void Update_RealTimeClock(void);
inline long long Get_RealTimeClock(void);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* APP_CONFIG_H */

