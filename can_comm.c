/*
 * File:   can_comm.c
 * Author: 
 *
 */

#include "mcc_generated_files/ecan.h"
#include "app_config.h"
#include "can_comm.h"

uCAN_MSG rxMessage;

bool CAN_Comm_Buffer_Empty(void) {
    return CAN_messagesInBuffer() == 0;
}

void CAN_Comm_Handle_RX_Message(void) {
    if (CAN_receive(&rxMessage)) {
        if (rxMessage.frame.idType == dSTANDARD_CAN_MSG_ID_2_0B) {
            switch(rxMessage.frame.id) {
                case CAN_RX_FULLPWR_ON_STDID:
                    Set_Event(EVENT_FULL_POWER_REQUEST);
                    Set_Wakeup_Source(WAKEUP_SOURCE_CAN_RX_0X55F);
                    break;
                case CAN_RX_BYCON_STDID:
                    if (rxMessage.frame.data1 == CAN_RX_BYCON_DATA1_ON) {
                        Set_Event(EVENT_BYCON_ON_REQUEST);
                    }
                    else if (rxMessage.frame.data1 == CAN_RX_BYCON_DATA1_OFF) {
                        Set_Event(EVENT_BYCON_OFF_REQUEST);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
